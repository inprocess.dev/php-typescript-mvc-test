<?php
require_once __DIR__ . '/vendor/autoload.php';

use App\Router;

$inDevelopmentMode = true;

if ($inDevelopmentMode) {
    $whoops = new \Whoops\Run;
    $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
    $whoops->register();
}

$router = new Router();

$router->add('', ['controller' => 'HomeController', 'action' => 'index']);
$router->add('parking', ['controller' => 'ParkingAreasController', 'action' => 'index']);
$router->add('parking/create', ['controller' => 'ParkingAreasController', 'action' => 'create']);
$router->add('parking/edit/{id:\d+}', ['controller' => 'ParkingAreasController', 'action' => 'edit']);
$router->add('parking/delete/{id:\d+}', ['controller' => 'ParkingAreasController', 'action' => 'delete']);
$router->add('payment', ['controller' => 'PaymentController', 'action' => 'index']);
$router->add('payment/calculate', ['controller' => 'PaymentController', 'action' => 'calculatePayment']);
$router->add('migrate', ['controller' => 'MigrationController', 'action' => 'createTables']);

$router->run();