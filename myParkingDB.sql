-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 24, 2024 at 11:11 AM
-- Server version: 5.7.39-log
-- PHP Version: 8.0.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myParkingDB`
--

-- --------------------------------------------------------

--
-- Table structure for table `exchangeRates`
--

CREATE TABLE `exchangeRates` (
  `id` int(6) UNSIGNED NOT NULL,
  `rateDate` date NOT NULL,
  `USD` decimal(10,4) NOT NULL,
  `EUR` decimal(10,4) NOT NULL,
  `PLN` decimal(10,4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `exchangeRates`
--

INSERT INTO `exchangeRates` (`id`, `rateDate`, `USD`, `EUR`, `PLN`) VALUES
(3, '2024-04-23', '1.0000', '0.9344', '4.0267'),
(4, '2024-04-24', '1.0000', '0.9343', '4.0263');

-- --------------------------------------------------------

--
-- Table structure for table `parkingAreas`
--

CREATE TABLE `parkingAreas` (
  `id` int(6) UNSIGNED NOT NULL,
  `parkingAreaName` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `weekdayRate` decimal(10,2) NOT NULL,
  `weekendRate` decimal(10,2) NOT NULL,
  `discountPercentage` int(11) NOT NULL,
  `reg_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `parkingAreas`
--

INSERT INTO `parkingAreas` (`id`, `parkingAreaName`, `weekdayRate`, `weekendRate`, `discountPercentage`, `reg_date`) VALUES
(1, 'area 1', '3.00', '4.00', 11, '2024-04-24 07:20:36'),
(2, 'area 2', '2.00', '3.00', 8, '2024-04-24 07:21:02'),
(4, 'area 3', '5.00', '8.00', 50, '2024-04-24 07:37:24');

-- --------------------------------------------------------

--
-- Table structure for table `parkingSessions`
--

CREATE TABLE `parkingSessions` (
  `id` int(6) UNSIGNED NOT NULL,
  `parkingAreaId` int(6) UNSIGNED NOT NULL,
  `startTime` datetime NOT NULL,
  `endTime` datetime NOT NULL,
  `parkingDay` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `exchangeRates`
--
ALTER TABLE `exchangeRates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parkingAreas`
--
ALTER TABLE `parkingAreas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parkingSessions`
--
ALTER TABLE `parkingSessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parkingAreaId` (`parkingAreaId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `exchangeRates`
--
ALTER TABLE `exchangeRates`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `parkingAreas`
--
ALTER TABLE `parkingAreas`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `parkingSessions`
--
ALTER TABLE `parkingSessions`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `parkingSessions`
--
ALTER TABLE `parkingSessions`
  ADD CONSTRAINT `parkingsessions_ibfk_1` FOREIGN KEY (`parkingAreaId`) REFERENCES `parkingAreas` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
