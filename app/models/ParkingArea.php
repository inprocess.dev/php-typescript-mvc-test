<?php

namespace App\Models;

class ParkingArea extends BaseModel {
    public function getAllParkingAreas() {
        return $this->read("SELECT * FROM parkingAreas");
    }

    public function getParkingAreaById($id) {
        return $this->read("SELECT * FROM parkingAreas WHERE id = ?", [$id]);
    }

    public function createParkingArea($data) {
        $sql = "INSERT INTO parkingAreas (parkingAreaName, weekdayRate, weekendRate, discountPercentage) VALUES (?, ?, ?, ?)";
        return $this->create($sql, [
            $data['parkingAreaName'],
            $data['weekdayRate'],
            $data['weekendRate'],
            $data['discountPercentage']
        ]);
    }

    public function updateParkingArea($id, $data) {
        $sql = "UPDATE parkingAreas SET parkingAreaName = ?, weekdayRate = ?, weekendRate = ?, discountPercentage = ? WHERE id = ?";
        return $this->update($sql, [
            $data['parkingAreaName'],
            $data['weekdayRate'],
            $data['weekendRate'],
            $data['discountPercentage'],
            $id
        ]);
    }

    public function deleteParkingArea($id) {
        return $this->delete("DELETE FROM parkingAreas WHERE id = ?", [$id]);
    }

    public function getAvailableParkingAreas() {
        $now = date('Y-m-d H:i:s');
        $sql = "SELECT pa.* FROM parkingAreas pa
                WHERE NOT EXISTS (
                    SELECT 1 FROM parkingSessions ps
                    WHERE ps.parkingAreaId = pa.id
                    AND ps.startTime <= ?
                    AND ps.endTime >= ?
                )";
        return $this->read($sql, [$now, $now]);
    }
}