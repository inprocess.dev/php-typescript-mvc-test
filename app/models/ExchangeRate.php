<?php

namespace App\Models;

class ExchangeRate extends BaseModel {
    public function getRateByDate($date) {
        $sql = "SELECT * FROM exchangeRates WHERE rateDate = ?";
        $result = $this->read($sql, [$date]);
        return $result ? $result[0] : null;
    }

    public function createRate($date, $rates) {
        $sql = "INSERT INTO exchangeRates (rateDate, USD, EUR, PLN) VALUES (?, ?, ?, ?)";
        $this->create($sql, [$date, $rates['USD'], $rates['EUR'], $rates['PLN']]);
    }
}
