<?php

namespace App\Models;

abstract class BaseModel {
    protected $pdo;

    public function __construct() {
        $this->connectToDb();
    }

    private function connectToDb() {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "myParkingDB";

        try {
            $this->pdo = new \PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $e) {
            die("Connection failed: " . $e->getMessage());
        }
    }

    protected function create($sql, $params) {
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($params);
        return $this->pdo->lastInsertId();
    }

    protected function read($sql, $params = []) {
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($params);
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    protected function update($sql, $params) {
        $stmt = $this->pdo->prepare($sql);
        return $stmt->execute($params);
    }

    protected function delete($sql, $params) {
        $stmt = $this->pdo->prepare($sql);
        return $stmt->execute($params);
    }
}
