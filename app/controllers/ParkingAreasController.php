<?php

namespace App\Controllers;

class ParkingAreasController extends Controller {
    public function index() {

        $parkingAreaModel = $this->model('ParkingArea');
        $parkingAreas = $parkingAreaModel->getAllParkingAreas();

        $data = [
            'title' => 'Parking Areas Management - Parking Lot Management System',
            'header' => 'Parking Areas Management',
            'parkingAreas' => $parkingAreas
        ];

        $this->view('parkingAreas/index', $data);
    }

    public function create() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $parkingAreaName = $_POST['parkingAreaName'];
            $weekdayRate = $_POST['weekdayRate'];
            $weekendRate = $_POST['weekendRate'];
            $discountPercentage = $_POST['discount'];

            $parkingAreaModel = $this->model('ParkingArea');
            $newParkingAreaId = $parkingAreaModel->createParkingArea([
                'parkingAreaName' => $parkingAreaName,
                'weekdayRate' => $weekdayRate,
                'weekendRate' => $weekendRate,
                'discountPercentage' => $discountPercentage
            ]);

           header('Content-Type: application/json');

            if ($newParkingAreaId) {
                $newParkingArea = $parkingAreaModel->getParkingAreaById($newParkingAreaId);
                echo json_encode(['success' => true, 'parkingArea' => $newParkingArea]);
            } else {
                echo json_encode(['success' => false, 'error' => 'Failed to create parking area.']);
            }
            exit;
        }
    }

    public function edit($id) {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $parkingAreaName = $_POST['parkingAreaName'];
            $weekdayRate = $_POST['weekdayRate'];
            $weekendRate = $_POST['weekendRate'];
            $discountPercentage = $_POST['discount'];

            $parkingAreaModel = $this->model('ParkingArea');
            $isUpdated = $parkingAreaModel->updateParkingArea($id, [
                'parkingAreaName' => $parkingAreaName,
                'weekdayRate' => $weekdayRate,
                'weekendRate' => $weekendRate,
                'discountPercentage' => $discountPercentage
            ]);

            header('Content-Type: application/json');

            if ($isUpdated) {
                $updatedParkingArea = $parkingAreaModel->getParkingAreaById($id);
                echo json_encode(['success' => true, 'parkingArea' => $updatedParkingArea]);
            } else {
                echo json_encode(['success' => false, 'error' => 'Failed to update parking area.']);
            }
            exit;
        }
    }

    public function delete($id) {
        if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
            $parkingAreaModel = $this->model('ParkingArea');
            $result = $parkingAreaModel->deleteParkingArea($id);

            header('Content-Type: application/json');
            if ($result) {
                echo json_encode(['success' => true]);
            } else {
                echo json_encode(['success' => false, 'error' => 'Failed to delete parking area.']);
            }
            exit;
        }
    }
}