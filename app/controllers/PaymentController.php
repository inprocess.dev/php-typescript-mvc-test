<?php

namespace App\Controllers;

use DateTime;

class PaymentController extends Controller
{
    public function index()
    {
        $parkingAreaModel = $this->model('ParkingArea');
        $parkingAreas = $parkingAreaModel->getAvailableParkingAreas();

        $data = [
            'title' => 'Calculate Parking Fee',
            'header' => 'Calculate Parking Fee',
            'parkingAreas' => $parkingAreas
        ];

        $this->view('payment/index', $data);
    }

    public function calculatePayment()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $parkingAreaId = $_POST['selectedParkingArea'];
            $startTime = $_POST['startTime'];
            $endTime = $_POST['endTime'];
            $parkingDay = $_POST['parkingDay'];
            $currency = $_POST['currency'];

            $parkingAreaModel = $this->model('ParkingArea');
            $parkingArea = $parkingAreaModel->getParkingAreaById($parkingAreaId)[0];

            $startDateTime = new DateTime("$parkingDay $startTime");
            $endDateTime = new DateTime("$parkingDay $endTime");

            if ($endDateTime < $startDateTime) {
                // Add one day to the end date if it's earlier than start date
                $endDateTime->modify('+1 day');
            }

            $interval = $startDateTime->diff($endDateTime);

            $time = $interval->h + ($interval->i / 60);

            $dayOfWeek = date('N', strtotime($parkingDay));
            $isWeekend = in_array($dayOfWeek, [6, 7]);

            $rate = $isWeekend ? $parkingArea['weekendRate'] : $parkingArea['weekdayRate'];
            $cost = $rate * $time;

            if ($currency !== 'USD') {
                $exchangeRates = $this->getExchangeRates();
                $cost = $cost * $exchangeRates[$currency];
            }

            $cost = round($cost, 2);

            $originalCost = $cost;

            if ($parkingArea['discountPercentage'] > 0) {
                $discount = ($cost * $parkingArea['discountPercentage']) / 100;
                $discount = round($discount, 2);
                $cost -= $discount;
            }

            header('Content-Type: application/json');
            echo json_encode(['amount' => $cost, 'originalAmount' => $originalCost, 'discount' => $parkingArea['discountPercentage']]);
            exit;
        }
    }

    public function getExchangeRates()
    {
        $date = date('Y-m-d');

        $exchangeRateModel = $this->model('ExchangeRate');
        $rate = $exchangeRateModel->getRateByDate($date);

        if (!$rate) {
            $apiKey = '395949df842173234027d833fc6b0589';
            $endpoint = 'http://api.exchangeratesapi.io/latest?access_key=' . $apiKey . '&symbols=USD,EUR,PLN';

            $response = file_get_contents($endpoint);

            if ($response === FALSE) {
                die('Failed to fetch data from exchange rates API.');
            }

            $responseData = json_decode($response, true);

            if (isset($responseData['error'])) {
                die('Error occurred while fetching exchange rates.');
            }

            $rates = $responseData['rates'];

            // As the base parameter is not available in the free version of the API,
            // we need to convert EUR base rates to USD base rates manually.
            $usdBaseRates = [
                'USD' => 1,
                'EUR' => 1 / $rates['USD'],
                'PLN' => $rates['PLN'] / $rates['USD']
            ];

            $exchangeRateModel->createRate($date, $usdBaseRates);

            return $usdBaseRates;
        }

        return [
            'USD' => $rate['USD'],
            'EUR' => $rate['EUR'],
            'PLN' => $rate['PLN']
        ];
    }
}