<?php

namespace App\Controllers;

abstract class Controller {
    protected function model($model) {
        $modelPath = 'App\\Models\\' . $model;
        $projectRootPath = dirname(__DIR__);
        require_once $projectRootPath . "/models/" . $model . ".php";
        return new $modelPath();
    }

    protected function view($view, $data = []) {
    extract($data);
    $projectRootPath = dirname(__DIR__);
    require_once $projectRootPath."/views/" . $view . ".php";
    }
}