<?php

namespace App\Controllers;

use mysqli;

class MigrationController extends Controller
{
    public function createTables()
    {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "myParkingDB";

        $conn = new mysqli($servername, $username, $password, $dbname);

        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        $sqlParkingAreas = "CREATE TABLE IF NOT EXISTS parkingAreas (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    parkingAreaName VARCHAR(30) NOT NULL,
    weekdayRate DECIMAL(10, 2) NOT NULL,
    weekendRate DECIMAL(10, 2) NOT NULL,
    discountPercentage INT NOT NULL,
    reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)";

        $sqlParkingSessions = "CREATE TABLE IF NOT EXISTS parkingSessions (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    parkingAreaId INT(6) UNSIGNED NOT NULL,
    startTime DATETIME NOT NULL,
    endTime DATETIME NOT NULL,
    parkingDay DATE NOT NULL,
    FOREIGN KEY (parkingAreaId) REFERENCES parkingAreas(id)
)";

        $sqlExchangeRates = "CREATE TABLE IF NOT EXISTS exchangeRates (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    rateDate DATE NOT NULL,
    USD DECIMAL(10, 4) NOT NULL,
    EUR DECIMAL(10, 4) NOT NULL,
    PLN DECIMAL(10, 4) NOT NULL
)";

        if ($conn->query($sqlParkingAreas) === TRUE) {
            echo "Table parkingAreas created successfully\n";
        } else {
            echo "Error creating table: " . $conn->error;
        }

        if ($conn->query($sqlParkingSessions) === TRUE) {
            echo "Table parkingSessions created successfully\n";
        } else {
            echo "Error creating table: " . $conn->error;
        }

        if ($conn->query($sqlExchangeRates) === TRUE) {
            echo "Table exchangeRates created successfully\n";
        } else {
            echo "Error creating table: " . $conn->error;
        }

        $conn->close();
    }
}
