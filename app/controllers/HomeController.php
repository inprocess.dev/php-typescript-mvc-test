<?php

namespace App\Controllers;

class HomeController extends Controller {
    public function index() {

        $data = [
            'title' => 'Parking Lot Management System',
            'header' => 'Parking Lot Management System'
        ];

        $this->view('home/index', $data);
    }
}