<?php include __DIR__ . '/../header.php'; ?>

<div class="main-content">
    <h2>Add Parking Area</h2>
    <form id="parkingAreaForm" action="/parking/create" method="POST">
        <label for="parkingAreaName">Parking Area Name:</label>
        <input type="text" id="parkingAreaName" name="parkingAreaName" required>

        <label for="weekdayRate">Weekdays Hourly Rate (USD):</label>
        <input type="number" id="weekdayRate" name="weekdayRate" step="0.01" min="0" required>

        <label for="weekendRate">Weekend Hourly Rate (USD):</label>
        <input type="number" id="weekendRate" name="weekendRate" step="0.01" min="0" required>

        <label for="discount">Discount Percentage (%):</label>
        <input type="number" id="discount" name="discount" step="1" min="0" max="100" required>

        <button type="submit" id="submitForm">Submit</button>
    </form>

    <h2>Existing Parking Areas</h2>
    <table>
        <thead>
        <tr>
            <th>Parking Area Name</th>
            <th>Weekdays Hourly Rate (USD)</th>
            <th>Weekend Hourly Rate (USD)</th>
            <th>Discount Percentage (%)</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($parkingAreas as $area): ?>
            <tr>
                <td><?php echo htmlspecialchars($area['parkingAreaName']); ?></td>
                <td><?php echo htmlspecialchars($area['weekdayRate']); ?></td>
                <td><?php echo htmlspecialchars($area['weekendRate']); ?></td>
                <td><?php echo htmlspecialchars($area['discountPercentage']); ?>%</td>
                <td>
                    <a href="#" class="edit-link" data-id="<?php echo $area['id']; ?>">Edit</a> |
                    <a href="#" class="delete-link" data-id="<?php echo $area['id']; ?>">Delete</a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>

<div id="editModal" class="modal">
    <div class="modal-content">
        <span class="close">&times;</span>
        <form id="editForm">
            <label>Parking Area Name:</label>
            <input type="text" id="parkingAreaName" name="parkingAreaName" required>

            <label>Weekdays Hourly Rate (USD):</label>
            <input type="number" id="weekdayRate" name="weekdayRate" step="0.01" min="0" required>

            <label>Weekend Hourly Rate (USD):</label>
            <input type="number" id="weekendRate" name="weekendRate" step="0.01" min="0" required>

            <label>Discount Percentage (%):</label>
            <input type="number" id="discount" name="discount" step="1" min="0" max="100" required>

            <button type="submit" id="submitForm">Submit</button>
        </form>
    </div>
</div>

<?php include __DIR__ . '/../footer.php'; ?>

<script src="/public/parkingArea.js"></script>
</body>
</html>