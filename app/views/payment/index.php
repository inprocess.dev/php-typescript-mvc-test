<?php include __DIR__ . '/../header.php'; ?>

    <div class="main-content">
        <h2>Parking Payment Calculator</h2>
        <form id="paymentCalculatorForm" action="/payment/calculate">
            <label for="selectedParkingArea">Select Parking Area:</label>
            <select id="selectedParkingArea" name="selectedParkingArea" required>
                <?php foreach ($parkingAreas as $area): ?>
                    <option value="<?php echo $area['id']; ?>">
                        <?php
                        echo htmlspecialchars($area['parkingAreaName']) . " - from $" .
                            number_format($area['weekdayRate'], 2) . "/hr";
                        ?>
                    </option>
                <?php endforeach; ?>
            </select>

            <label for="startTime">Start Time:</label>
            <input type="time" id="startTime" name="startTime" required value="<?php
            $timezone = new DateTimeZone('Europe/Warsaw');
            $dateTime = new DateTime('now', $timezone);
            echo $dateTime->format('H:i'); ?>">

            <label for="endTime">End Time:</label>
            <input type="time" id="endTime" name="endTime" required>

            <label for="parkingDay">Parking Day:</label>
            <input type="date" id="parkingDay" name="parkingDay" required value="<?php echo date('Y-m-d'); ?>">

            <label for="currency">Currency:</label>
            <select id="currency" name="currency" required>
                <option value="USD" selected>USD</option>
                <option value="EUR">EUR</option>
                <option value="PLN">PLN</option>
            </select>

            <button type="submit" id="calculatePayment">Calculate</button>
        </form>

        <div id="paymentResult">

        </div>

        <button id="pay-button">Pay</button>
    </div>

<?php include __DIR__ . '/../footer.php'; ?>

<script src="/public/paymentCalculator.js"></script>
</body>
</html>
