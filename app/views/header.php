<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" href="/public/styles.css">
</head>
<body>
<div class="header">
    <h1><?php echo $header; ?></h1>
</div>
<div class="nav">
    <a href="/">Home</a>
    <a href="/parking">Manage Parking Areas</a>
    <a href="/payment">Calculate Parking Fee</a>
</div>
