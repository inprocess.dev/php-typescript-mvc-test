document.addEventListener('DOMContentLoaded', function () {
    var parkingAreaForm = document.getElementById('parkingAreaForm');
    parkingAreaForm.addEventListener('submit', function (e) { return handleFormSubmit(e, parkingAreaForm); });
    document.querySelectorAll('.delete-link').forEach(function (link) {
        link.addEventListener('click', handleDeleteClick);
    });
    document.querySelectorAll('.edit-link').forEach(function (link) {
        link.addEventListener('click', function (e) {
            e.preventDefault();
            openEditModal(this);
        });
    });
    var closeButton = document.querySelector('.close');
    if (closeButton) {
        closeButton.addEventListener('click', closeModal);
    }
});
function handleFormSubmit(e, parkingAreaForm) {
    e.preventDefault();
    var formData = new FormData(parkingAreaForm);
    fetch(parkingAreaForm.action, {
        method: 'POST',
        body: formData
    })
        .then(function (response) { return response.json(); })
        .then(function (data) {
        if (data.success && data.parkingArea && data.parkingArea.length > 0) {
            var area = data.parkingArea[0];
            var table = document.querySelector('table tbody');
            var newRow = table.insertRow();
            newRow.innerHTML = "\n                <td>".concat(area.parkingAreaName, "</td>\n                <td>").concat(area.weekdayRate, "</td>\n                <td>").concat(area.weekendRate, "</td>\n                <td>").concat(area.discountPercentage, "%</td>\n                <td>\n                    <a href=\"#\" class=\"edit-link\" data-id=\"").concat(area.id, "\">Edit</a> |\n                    <a href=\"#\" class=\"delete-link\" data-id=\"").concat(area.id, "\">Delete</a>\n                </td>\n            ");
            var editLink = newRow.querySelector('.edit-link');
            if (editLink) {
                editLink.addEventListener('click', function (e) {
                    e.preventDefault();
                    openEditModal(this);
                });
            }
            var deleteLink = newRow.querySelector('.delete-link');
            if (deleteLink) {
                deleteLink.addEventListener('click', handleDeleteClick);
            }
            parkingAreaForm.reset();
            console.log('Parking area created successfully!');
        }
        else {
            console.error('An error occurred:', data.error);
        }
    })
        .catch(function (error) { return console.error('Error:', error); });
}
function handleDeleteClick(e) {
    e.preventDefault();
    var link = e.target;
    if (confirm('Are you sure you want to delete this entry?')) {
        var row_1 = link.closest('tr');
        var id = link.getAttribute('data-id');
        fetch("/parking/delete/".concat(id), {
            method: 'DELETE',
        })
            .then(function (response) { return response.json(); })
            .then(function (data) {
            if (data.success) {
                if (row_1) {
                    row_1.remove();
                }
                console.log('Entry deleted successfully!');
            }
            else {
                console.error('Error deleting:', data.error);
            }
        })
            .catch(function (error) { return console.error('Error:', error); });
    }
}
function openEditModal(link) {
    var modal = document.getElementById('editModal');
    modal.style.display = 'block';
    var parkingAreaRow = link.closest('tr');
    if (parkingAreaRow) {
        var editForm_1 = document.getElementById('editForm');
        populateForm(editForm_1, parkingAreaRow);
        editForm_1.onsubmit = function (e) {
            e.preventDefault();
            updateDataOnServer(link, editForm_1, parkingAreaRow);
        };
    }
}
function populateForm(editForm, parkingAreaRow) {
    var _a;
    var parkingAreaName = parkingAreaRow.cells[0].textContent;
    var weekdayRate = parkingAreaRow.cells[1].textContent;
    var weekendRate = parkingAreaRow.cells[2].textContent;
    var discountPercentage = (_a = parkingAreaRow.cells[3].textContent) === null || _a === void 0 ? void 0 : _a.replace('%', '');
    editForm.parkingAreaName.value = parkingAreaName || '';
    editForm.weekdayRate.value = weekdayRate || '';
    editForm.weekendRate.value = weekendRate || '';
    editForm.discount.value = discountPercentage || '';
}
function updateDataOnServer(link, editForm, parkingAreaRow) {
    var id = link.getAttribute('data-id');
    var formData = new FormData(editForm);
    fetch("/parking/edit/".concat(id), {
        method: 'POST',
        body: formData
    })
        .then(function (response) { return response.json(); })
        .then(function (data) {
        if (data.success) {
            var updatedArea = data.parkingArea ? data.parkingArea[0] : null;
            if (updatedArea && parkingAreaRow) {
                parkingAreaRow.cells[0].textContent = updatedArea.parkingAreaName;
                parkingAreaRow.cells[1].textContent = updatedArea.weekdayRate;
                parkingAreaRow.cells[2].textContent = updatedArea.weekendRate;
                parkingAreaRow.cells[3].textContent = "".concat(updatedArea.discountPercentage, "%");
            }
            closeModal();
            highlightRow(parkingAreaRow);
            console.log('Parking area updated successfully!');
        }
        else {
            alert("Error: ".concat(data.error));
            console.error('An error occurred:', data.error);
        }
    })
        .catch(function (error) {
        alert('Network error, could not update parking area.');
        console.error('Error:', error);
    });
}
function closeModal() {
    var modal = document.getElementById('editModal');
    if (modal) {
        modal.style.display = 'none';
    }
}
function highlightRow(row) {
    row.classList.add('highlighted');
    setTimeout(function () { return row.classList.remove('highlighted'); }, 1000);
}
