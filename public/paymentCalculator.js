var PaymentCalculator = /** @class */ (function () {
    function PaymentCalculator(formId, resultId) {
        this.form = document.getElementById(formId);
        this.resultContainer = document.getElementById(resultId);
        this.form.addEventListener('submit', this.handleFormSubmit.bind(this));
    }
    PaymentCalculator.prototype.handleFormSubmit = function (event) {
        var _this = this;
        event.preventDefault();
        var formData = new FormData(this.form);
        this.currency = formData.get('currency');
        fetch(this.form.action, {
            method: 'POST',
            body: formData
        })
            .then(function (response) {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
            .then(function (data) {
            _this.displayResult(data);
        })
            .catch(function (error) {
            console.error('There has been a problem with your fetch operation:', error);
            _this.resultContainer.textContent = 'Error calculating payment.';
        });
    };
    PaymentCalculator.prototype.displayResult = function (data) {
        var amount = data.amount.toFixed(2);
        var originalAmount = data.originalAmount.toFixed(2);
        var amountString;
        var oldAmountString;
        switch (this.currency) {
            case 'USD':
                amountString = "$".concat(amount);
                oldAmountString = "$".concat(originalAmount);
                break;
            case 'EUR':
                amountString = "\u20AC".concat(amount);
                oldAmountString = "\u20AC".concat(originalAmount);
                break;
            case 'PLN':
                amountString = "".concat(amount, "z\u0142");
                oldAmountString = "".concat(originalAmount, "z\u0142");
                break;
            default:
                amountString = "$".concat(amount);
                oldAmountString = "$".concat(originalAmount);
        }
        if (data.discount > 0) {
            this.resultContainer.innerHTML = "<del>Old Price: ".concat(oldAmountString, "</del> <b>New Price: ").concat(amountString, "</b> <small>Discount: ").concat(data.discount, "%</small>");
        }
        else {
            this.resultContainer.textContent = "Amount to pay: ".concat(amountString);
        }
        this.showPayButton();
    };
    PaymentCalculator.prototype.showPayButton = function () {
        var payButton = document.getElementById('pay-button');
        if (payButton) {
            payButton.style.display = 'block';
        }
    };
    return PaymentCalculator;
}());
document.addEventListener('DOMContentLoaded', function () {
    new PaymentCalculator('paymentCalculatorForm', 'paymentResult');
});
