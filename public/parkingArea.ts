interface ParkingArea {
    id: string;
    parkingAreaName: string;
    weekdayRate: string;
    weekendRate: string;
    discountPercentage: string;
    reg_date: string;
}

interface ServerResponse {
    success: boolean;
    parkingArea?: ParkingArea[];
    error?: string;
}

document.addEventListener('DOMContentLoaded', () => {
    const parkingAreaForm = document.getElementById('parkingAreaForm') as HTMLFormElement;

    parkingAreaForm.addEventListener('submit', (e: Event) => handleFormSubmit(e, parkingAreaForm));

    document.querySelectorAll('.delete-link').forEach(link => {
        link.addEventListener('click', handleDeleteClick);
    });

    document.querySelectorAll('.edit-link').forEach(link => {
        link.addEventListener('click', function (e) {
            e.preventDefault();
            openEditModal(this);
        });
    });

    const closeButton = document.querySelector('.close') as HTMLElement;
    if (closeButton) {
        closeButton.addEventListener('click', closeModal);
    }
});

function handleFormSubmit(e: Event, parkingAreaForm: HTMLFormElement) {
    e.preventDefault();
    const formData = new FormData(parkingAreaForm);

    fetch(parkingAreaForm.action, {
        method: 'POST',
        body: formData
    })
        .then(response => response.json() as Promise<ServerResponse>)
        .then(data => {
            if (data.success && data.parkingArea && data.parkingArea.length > 0) {
                const area = data.parkingArea[0];
                const table = document.querySelector('table tbody') as HTMLTableSectionElement;
                const newRow = table.insertRow();
                newRow.innerHTML = `
                <td>${area.parkingAreaName}</td>
                <td>${area.weekdayRate}</td>
                <td>${area.weekendRate}</td>
                <td>${area.discountPercentage}%</td>
                <td>
                    <a href="#" class="edit-link" data-id="${area.id}">Edit</a> |
                    <a href="#" class="delete-link" data-id="${area.id}">Delete</a>
                </td>
            `;
                const editLink = newRow.querySelector('.edit-link');
                if (editLink) {
                    editLink.addEventListener('click', function (e) {
                        e.preventDefault();
                        openEditModal(this);
                    });
                }
                const deleteLink = newRow.querySelector('.delete-link');
                if (deleteLink) {
                    deleteLink.addEventListener('click', handleDeleteClick);
                }
                parkingAreaForm.reset();
                console.log('Parking area created successfully!');
            } else {
                console.error('An error occurred:', data.error);
            }
        })
        .catch(error => console.error('Error:', error));
}


function handleDeleteClick(e) {
    e.preventDefault();
    const link = e.target;

    if (confirm('Are you sure you want to delete this entry?')) {
        const row = link.closest('tr');
        const id = link.getAttribute('data-id');

        fetch(`/parking/delete/${id}`, {
            method: 'DELETE',
        })
            .then(response => response.json())
            .then(data => {
                if (data.success) {
                    if (row) {
                        row.remove();
                    }
                    console.log('Entry deleted successfully!');
                } else {
                    console.error('Error deleting:', data.error);
                }
            })
            .catch(error => console.error('Error:', error));
    }
}

function openEditModal(link: Element): void {
    const modal = document.getElementById('editModal') as HTMLElement;
    modal.style.display = 'block';

    const parkingAreaRow = link.closest('tr') as HTMLTableRowElement;

    if (parkingAreaRow) {
        const editForm = document.getElementById('editForm') as HTMLFormElement;

        populateForm(editForm, parkingAreaRow);

        editForm.onsubmit = function (e: Event) {
            e.preventDefault();
            updateDataOnServer(link, editForm, parkingAreaRow);
        };
    }
}

function populateForm(editForm: HTMLFormElement, parkingAreaRow: HTMLTableRowElement): void {
    const parkingAreaName = parkingAreaRow.cells[0].textContent;
    const weekdayRate = parkingAreaRow.cells[1].textContent;
    const weekendRate = parkingAreaRow.cells[2].textContent;
    const discountPercentage = parkingAreaRow.cells[3].textContent?.replace('%', '');

    editForm.parkingAreaName.value = parkingAreaName || '';
    editForm.weekdayRate.value = weekdayRate || '';
    editForm.weekendRate.value = weekendRate || '';
    editForm.discount.value = discountPercentage || '';
}

function updateDataOnServer(link: Element, editForm: HTMLFormElement, parkingAreaRow: HTMLTableRowElement): void {
    const id = link.getAttribute('data-id');
    const formData = new FormData(editForm);

    fetch(`/parking/edit/${id}`, {
        method: 'POST',
        body: formData
    })
        .then(response => response.json() as Promise<ServerResponse>)
        .then((data: ServerResponse) => {
            if (data.success) {
                const updatedArea = data.parkingArea ? data.parkingArea[0] : null;
                if (updatedArea && parkingAreaRow) {
                    parkingAreaRow.cells[0].textContent = updatedArea.parkingAreaName;
                    parkingAreaRow.cells[1].textContent = updatedArea.weekdayRate;
                    parkingAreaRow.cells[2].textContent = updatedArea.weekendRate;
                    parkingAreaRow.cells[3].textContent = `${updatedArea.discountPercentage}%`;
                }
                closeModal();

                highlightRow(parkingAreaRow);

                console.log('Parking area updated successfully!');
            } else {
                alert(`Error: ${data.error}`);
                console.error('An error occurred:', data.error);
            }
        })
        .catch(error => {
            alert('Network error, could not update parking area.');
            console.error('Error:', error);
        });
}

function closeModal(): void {
    const modal = document.getElementById('editModal') as HTMLElement;
    if (modal) {
        modal.style.display = 'none';
    }
}

function highlightRow(row: HTMLTableRowElement): void {
    row.classList.add('highlighted');
    setTimeout(() => row.classList.remove('highlighted'), 1000);
}


