interface PaymentData {
    amount: number;
    originalAmount: number;
    discount: number;
}

class PaymentCalculator {
    private form: HTMLFormElement;
    private resultContainer: HTMLElement;
    private currency: string;

    constructor(formId: string, resultId: string) {
        this.form = document.getElementById(formId) as HTMLFormElement;
        this.resultContainer = document.getElementById(resultId) as HTMLElement;
        this.form.addEventListener('submit', this.handleFormSubmit.bind(this));
    }

    private handleFormSubmit(event: Event): void {
        event.preventDefault();
        const formData = new FormData(this.form);
        this.currency = formData.get('currency') as string;

        fetch(this.form.action, {
            method: 'POST',
            body: formData
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                return response.json();
            })
            .then((data: PaymentData) => {
                this.displayResult(data);
            })
            .catch(error => {
                console.error('There has been a problem with your fetch operation:', error);
                this.resultContainer.textContent = 'Error calculating payment.';
            });
    }

    private displayResult(data: PaymentData): void {
        const amount = data.amount.toFixed(2);
        const originalAmount = data.originalAmount.toFixed(2);
        let amountString: string;
        let oldAmountString: string;

        switch (this.currency) {
            case 'USD':
                amountString = `$${amount}`;
                oldAmountString = `$${originalAmount}`;
                break;
            case 'EUR':
                amountString = `€${amount}`;
                oldAmountString = `€${originalAmount}`;
                break;
            case 'PLN':
                amountString = `${amount}zł`;
                oldAmountString = `${originalAmount}zł`;
                break;
            default:
                amountString = `$${amount}`;
                oldAmountString = `$${originalAmount}`;
        }

        if (data.discount > 0) {
            this.resultContainer.innerHTML = `<del>Old Price: ${oldAmountString}</del> <b>New Price: ${amountString}</b> <small>Discount: ${data.discount}%</small>`;
        } else {
            this.resultContainer.textContent = `Amount to pay: ${amountString}`;
        }

        this.showPayButton();
    }

    private showPayButton(): void {
        const payButton = document.getElementById('pay-button');
        if (payButton) {
            payButton.style.display = 'block';
        }
    }
}

document.addEventListener('DOMContentLoaded', () => {
    new PaymentCalculator('paymentCalculatorForm', 'paymentResult');
});